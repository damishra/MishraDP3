# Server Programming Project 3

## How to use

1. Clone repository :

```bash
    git clone https://github.com/MishraDP3.git
```

2. cd into repo directory :

```
    cd MishraDP3
```

3. Install node dependencies :

```
    npm install
```

4. Run the server :

```
    node server.js
```
